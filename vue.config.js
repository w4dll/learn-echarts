module.exports = {
    lintOnSave: 'warning',
    devServer: {
        proxy: {
            '/':{
                target:'http://127.0.0.1:3000/api/',
                changeOrigin:true
            }
        },
        port:8080,
    },
    publicPath:'./'
}