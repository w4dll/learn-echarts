import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


const routes = [
    {path: '/', redirect:'/home'},
    {path: '/home', component: ()=>import('@/views/Home')},
    {path:'/sellerpage',component:()=>import('@/views/SellerPage')},
    {path:'/trendpage',component: ()=>import('@/views/TrendPage')},
    {path:'/mappage',component: ()=>import('@/views/MapePage')},
    {path:'/hotpage',component: ()=>import('@/views/HotPage')},
    {path:'/stockpage',component: ()=>import('@/views/StockPage')},
    {path:'/rankpage',component: ()=>import('@/views/RankPage')},
]

const router = new VueRouter(({
    routes
}))

export default router