export default class SocketService{
    // 单例模式
    static instance = null
    static get Instance (){ // get的时候不需要用括号获取
        if (!this.instance){
            this.instance = new SocketService()
        }
        return this.instance
    }

    // 服务端连接的对象
    ws = null
    connected = false
    sendRetryCount = 0 // 记录重试次数
    connectRetryCount = 0 // 重连次数

    callBackMapping = {}



    // 定义连接服务器方法
    connect(){
        if (!window.WebSocket){
            return console.log('浏览器不支持WebSocket')
        }
        this.ws = new WebSocket('ws://localhost:9998')

        // 连接成功的事件
        this.ws.onopen = ()=>{
            console.log('连接服务器成功')
            this.connected = true
            this.connectRetryCount = 0
        }

        // 连接失败
        // 1.连接服务器失败会调用
        // 2.服务器关闭了
        this.ws.onclose = ()=>{
            console.log('连接服务器失败')
            this.connected = false
            this.connectRetryCount++

            setTimeout(()=>{
                this.connect()
            }, 500*this.connectRetryCount)
        }

        // 消息响应
        this.ws.onmessage = msg=>{
            console.log('从服务端获取了数据')
            // console.log(msg.data)
            const recvData = JSON.parse(msg.data)
            const socketType = recvData.socketType
            if (this.callBackMapping[socketType]){
                const action = recvData.action
                if (action === 'getData'){
                    const realData = JSON.parse(recvData.data)
                    this.callBackMapping[socketType].call(this,realData)
                }else if (action === 'fullscreen'){
                    console.log('fullscreen')
                    this.callBackMapping[socketType].call(this, recvData)
                }else if (action === 'themeChange'){
                    console.log('themeChange')
                    this.callBackMapping[socketType].call(this, recvData)
                }
            }

        }
    }

    // 注册一个回调函数
    registerCallBack(socketType, callBack){
        this.callBackMapping[socketType] = callBack
    }
    // 取消某一个回调函数
    unRegisterCallBack(socketType){
        this.callBackMapping[socketType] = null
    }

    send(data){
        if (this.connected){
            this.sendRetryCount = 0
            this.ws.send(JSON.stringify(data))
        }else{
            this.sendRetryCount++
            setTimeout(()=>{
                this.send(data)
            },500*this.sendRetryCount)
        }

    }
}