import Vue from 'vue'
import App from './App.vue'
import router from "@/router";
import store from './store'

import axios from "axios";
import * as echarts from 'echarts'


import SocketService from "@/utils/socketService";
SocketService.Instance.connect()  // 服务端连接
Vue.prototype.$socket = SocketService.Instance

Vue.config.productionTip = false

// 注册全局主题
import darkTheme from '@/../data/theme/dark'
import chalkTheme from '@/../data/theme/chalk'

echarts.registerTheme('chalk', chalkTheme)
echarts.registerTheme('dark', darkTheme)

// 全局组件
Vue.prototype.$echarts = echarts


// 挂载全局axios
axios.defaults.url = 'http://127.0.0.1:3000'
Vue.prototype.$http = axios


import './assets/css/global.css'


new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
